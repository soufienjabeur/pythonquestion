## 1 Looping in python and best practices

To loop in my python there are two ways by using:
 * for loop
 * while loop


```python
# I will declare a liste of name and I will iterate through it:
List=['Sarah', 'Alex', 'Peter', 'Jack']
# for each loop without a counter
for ele in List:
    print(ele+"\n")
```

    Sarah
    
    Alex
    
    Peter
    
    Jack
    
    


```python
# for using in range
# len to get the length of the my List
for i in range(0, len(List)):
    print(List[i]+"\n")
```

    Sarah
    
    Alex
    
    Peter
    
    Jack
    
    


```python
# while loop using the classic way
i=0
while i < len(List):
    print(List[i]+"\n")
    i+=1
```

    Sarah
    
    Alex
    
    Peter
    
    Jack
    
    

## 2 How to manipulate strings (cut substring, find if substring exists ...)


Strings in python are like arrays, you can iterate over them and play different operations


```python
name="Alex"
for ele in name:
    print(ele)
```

    A
    l
    e
    x
    

for more operations about Strings you can find all of them in w3schools
https://www.w3schools.com/python/python_ref_string.asp

## 3 Floats and numbers are they the same ?

Python has five standard data types:
    * Numbers (int, long, float, complex)
    * Boolean(bool)
    * String
    * List
    * Tuple
    * Dictionary   

## 4 Operators and negation (=, !=, <, <= ...)


They are the same as java or javascript just a little  difference in logical operation and incrementation
find more about that here:
https://www.w3schools.com/python/python_operators.asp

## 5 What are dicts? Are they objects ?


* Python's dictionaries are kind of hash table type.
* It is generally used when we have a huge amount of data. Dictionaries are optimized for retrieving data. 
    We must know the key to retrieve the value.
* A dictionary key can be almost any Python type, but are usually numbers or strings. 
    Values, on the other hand, can be any arbitrary Python object.
* Dictionaries are enclosed by curly braces ({ }) and values can be assigned and accessed using square braces ([])


```python
Fruits={'Appel':'1.4$','Orange':'1.3$', 'Banana':'1.5$'}
```


```python
Fruits
```




    {'Appel': '1.4', 'Orange': '1.3', 'Banana': '1.5'}




```python
type(Fruits)
```




    dict




```python
# adding new element to my dic Fruits
Fruits['Watermelon']= '2$'
```


```python
Fruits
```




    {'Appel': '1.4', 'Orange': '1.3', 'Banana': '1.5', 'Watermelon': '2$'}




```python
# iterate over a dic
for ele in Fruits:
    #It will print for me only keys
    print(ele)
    #If I want to get the values:
    print(Fruits[ele])
```

    Appel
    1.4
    Orange
    1.3
    Banana
    1.5
    Watermelon
    2$
    


```python
# Getting keys and values separately
key_list = list(Fruits.keys())
print(key_list)
val_list = list(Fruits.values())
print(val_list)
```

    ['Appel', 'Orange', 'Banana', 'Watermelon']
    ['1.4', '1.3', '1.5', '2$']
    


```python
# Check of Orange's key exist in Fruits
if 'Orange' in Fruits:
    print('exist')
```

    exist
    

## 6 How to manipulate objects 

Python is an object-oriented programming language like java and typescript


```python
class Person:
    #Construter
    def __init__(self, name, age, sold):
        self.name=name
        self.age=age
        self.sold=sold
        
    #Simple function
    def addToAccount(self, amountOfMoney):
        self.sold+=amountOfMoney

```


```python
Alex=Person('Alex', 25, 1200.0)
```


```python
Alex.sold
```




    1200.0




```python
Alex.addToAccount(1200.0)
```


```python
Alex.sold
```




    2400.0




```python
class Employ(Person):
    
    #Constructeur
    def __init__(self, name, age, sold,entrepriseName):
        super().__init__(name,age,sold)
        self.entrepriseName=entrepriseName
        
    #Simple methode   
    def printEntrepiseName(self):
        print(self.entrepriseName)
```


```python
Peter=Employ('Peter', 30, 2500.0, 'Google')
```


```python
Peter.printEntrepiseName()
```

    Google
    


```python

```
